import os
import sqlite3

from flask import Flask, render_template, url_for, request, flash, g, redirect, make_response
from werkzeug.security import check_password_hash, generate_password_hash

from defort_1_0.flask_database import FlaskDataBase

DATABASE = "flaskapp.db"
DEBUG = True
SECRET_KEY = 'gheghgj3qhgt4q$#^#$he'
app = Flask(__name__)
app.config['SECRET_KEY'] = 'gheghgj3qhgt4q$#^#$he'
app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(DATABASE=os.path.join(app.root_path, 'flaskapp.db')))

fdb = None


@app.before_request
def services():
    global fdb
    fdb = FlaskDataBase(get_db())


def create_db():
    db = connect_db()
    with app.open_resource("db_schem.sql", "r") as f:
        db.cursor().executescript(f.read())
    db.commit()
    db.close()


def get_db():
    if not hasattr(g, "link_db"):
        g.link_db = connect_db()
    return g.link_db


def connect_db():
    conn = sqlite3.connect(app.config["DATABASE"])
    conn.row_factory = sqlite3.Row
    return conn


def only_for_logged_users(func):
    def wrapper(*args, **kwargs):
        if not request.cookies.get("logged"):
            return redirect(url_for("log_in"))
        return func(*args, **kwargs)

    wrapper.__name__ = func.__name__
    return wrapper


def only_for_unlogged_users(func):
    def wrapper(*args, **kwargs):
        if request.cookies.get("logged"):
            return redirect(url_for("index"))
        return func(*args, **kwargs)

    wrapper.__name__ = func.__name__
    return wrapper


def check_registration_form(email: str, pass1: str, pass2: str) -> str:
    if not (email and pass1 and pass2):
        return "All fields must be filled"

    required_symbols = ["@", "."]
    for symbol in required_symbols:
        if symbol not in email:
            return "Incorrect email"

    if pass1 != pass2:
        return "Password mismatch"

    if len(pass1) < 5 or pass1.upper == pass1 or pass1.lower() == pass1 or "123" in pass1 or "qwe" in pass1:
        return "Password to easy"

    email_exists = FlaskDataBase(get_db()).email_exist(email)
    if email_exists:
        return "User with current email already exists"

    return ""


def register_user(email, password):
    password_hash = generate_password_hash(password)
    fdb.add_new_user(email, password_hash)


def check_login_form(email: str, password: str) -> str:
    if not (email and password):
        return "All fields must be filled"

    user = FlaskDataBase(get_db()).get_user(email)
    if not user or not check_password_hash(user["password"], password):
        return "Login or Password incorrect"

    return ""


@app.route('/add_post', methods=['POST', 'GET'])
@only_for_logged_users
def add_post():
    logged = True if request.cookies.get("logged") else False
    if request.method == "POST":
        title = request.form["name"]
        description = request.form["post"]
        res = fdb.add_post(title, description)
        if not res:
            flash("POST WAS INCORRECT", category="error")
        else:
            flash("NICE", category="success")
    return render_template("posts/add_post.html", menu_url=fdb.get_menu(), logged=logged)


@app.route("/all_posts")
@only_for_logged_users
def all_posts():
    logged = True if request.cookies.get("logged") else False
    k = fdb.get_posts()
    print(dir(k[0]))
    print(k[0].keys())
    return render_template("posts/all_posts.html", menu_url=fdb.get_menu(), posts=fdb.get_posts(), logged=logged)


@app.route("/post/<int:post_id>")
@only_for_logged_users
def post_content(post_id):
    logged = True if request.cookies.get("logged") else False
    title, description = fdb.get_post_content(post_id)

    return render_template("posts/post_info.html", menu_url=fdb.get_menu(),
                           title=title, description=description, logged=logged)


@app.route('/')
def index():
    logged = True if request.cookies.get("logged") else False
    return render_template('index.html', menu_url=fdb.get_menu(),  logged=logged)


@app.route('/second')
def second():
    logged = True if request.cookies.get("logged") else False
    return render_template('second.html', menu_url=fdb.get_menu(),  logged=logged)


@app.route('/user/<username>')
def profile(username):
    return f"<h1>Hello {username}!</h1>"


@app.route('/log_in', methods=['POST', 'GET'])
@only_for_unlogged_users
def log_in():
    logged = True if request.cookies.get("logged") else False
    if request.method == 'GET':
        return render_template('authorization/login.html', menu_url=fdb.get_menu(),  logged=logged)

    email = request.form.get('email')
    password = request.form.get('password')
    error_message = check_login_form(email, password)
    if error_message:
        flash(error_message, category="error")
        return render_template('authorization/login.html', menu_url=fdb.get_menu(),  logged=logged)

    response = make_response(redirect(url_for("index")))
    response.set_cookie("logged", "yes")
    return response


@app.route("/sign_up", methods=["GET", "POST"])
@only_for_unlogged_users
def sign_up():
    logged = True if request.cookies.get("logged") else False
    if request.method == "GET":
        return render_template("authorization/sign_up.html", menu_url=fdb.get_menu(),  logged=logged)

    email = request.form.get('email')
    password = request.form.get('password')
    password2 = request.form.get("password2")
    error_message = check_registration_form(email, password, password2)
    if error_message:
        flash(error_message, category="error")
        return render_template("authorization/sign_up.html", menu_url=fdb.get_menu(),  logged=logged)

    register_user(email, password)
    response = make_response(redirect(url_for("index")))
    response.set_cookie("logged", "yes")
    return response


@app.route("/log_out")
@only_for_logged_users
def log_out():
    response = make_response(redirect(url_for("index")))
    response.delete_cookie("logged")
    return response


@app.errorhandler(404)
def page_not_found(error):
    return "<h1>Ooooopsi! This page does not exist!</h1>"


@app.teardown_appcontext
def close_db(error):
    """Close database connection if it exists."""
    if hasattr(g, 'link_db'):
        g.link_db.close()


if __name__ == '__main__':
    app.run(debug=True)
