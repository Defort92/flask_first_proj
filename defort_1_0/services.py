from defort_1_0.flask_database import FlaskDataBase

def check_registration_form(email: str, pass1: str, pass2: str) -> str:
    if not (email, pass1, pass2):
        return "All fields must be filled"

    required_symbols = ["@", "."]
    for symbol in required_symbols:
        if symbol not in email:
            return "Incorrect email"

    if pass1 != pass2:
        return "Password mismatch"

    if len(pass1) < 5 or pass1.upper == pass1 or pass1.lower() == pass1:
        return "Password to easy"

    email_exists = FlaskDataBase(get_db()).email_exist(email)
    if email_exists:
        return "User with current email already exists"

    return ""


def check_login_form(email: str, pas: str) -> str:
    if not (email, pas):
        return "All fields must be filled"

    user = FlaskDataBase(get_db()).get_user(email)
    if not user:
        return "Login or Password incorrect"

    return ""